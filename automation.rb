require 'json'
require 'open3'
require 'open-uri'
require 'uri'

MAX_PAGES = 500
PER_PAGE = 20
DEFAULT_PARAMS = "per_page=#{PER_PAGE}&include_subgroups=true&order_by=id&sort=asc"
DEFAULT_POLICY_PROJECT_BRANCH_NAME = 'main'

class GitlabApiService
  NEW_VERSION_WARNING = 'new version of glab has been released'

  GlabError = Class.new(StandardError)

  def list_projects_in_group(group_id)
    (1..MAX_PAGES).each do |page|
      results = call_glab_api("groups/#{group_id}/projects?#{DEFAULT_PARAMS}&page=#{page}").each do |project|
        next if project['empty_repo']

        yield project['id'], project['path_with_namespace'], project['default_branch']
      end

      break if results.length < PER_PAGE
    end
  end

  def assign_security_policy_project(full_path, security_policy_project_id)
    graphql_query = <<~GRAPHQL
      mutation {
        securityPolicyProjectAssign(input: { fullPath: "#{full_path}", securityPolicyProjectId: "gid://gitlab/Project/#{security_policy_project_id}" }) {
          errors
        }
      }
    GRAPHQL

    call_glab_api("graphql -f query='#{graphql_query}'")
  end

  def unassign_security_policy_project(full_path)
    graphql_query = <<~GRAPHQL
      mutation {
        securityPolicyProjectUnassign(input: { fullPath: "#{full_path}" }) {
          errors
        }
      }
    GRAPHQL

    call_glab_api("graphql -f query='#{graphql_query}'", ignore_errors: true)
  end

  def has_gitlab_ci_file?(project_id, default_branch)
    result = call_glab_api("projects/#{project_id}/repository/files/.gitlab-ci.yml?ref=#{default_branch}")
    !result['size'].nil? && result['size'].to_i > 0
  rescue GlabError
    false
  end

  private

  def call_glab_api(command, ignore_errors: false)
    log_info "glab api #{command}"
    stdout, stderr, status = Open3.capture3("glab api #{command}")
    log_info success?(status, stderr) ? "   ... success!" : "   ... failed (error: #{stderr.strip}) :("
    return {} if success?(status, stderr) && (stdout.nil? || stdout.empty?)
    return JSON.parse(stdout) if success?(status, stderr)

    raise GlabError, stderr unless ignore_errors
  end

  def success?(status, stderr)
    status.success? && (stderr.strip.empty? || stderr.include?(NEW_VERSION_WARNING))
  end

  def log_info(message)
    return if ENV['DEBUG'].nil?

    puts message
  end
end

class AutoAssignSecurityPolicyProject
  def initialize(gitlab_api_service:)
    @gitlab_api_service = gitlab_api_service
  end

  def execute(group_id, security_policy_project_id)
    gitlab_api_service.list_projects_in_group(group_id) do |project_id, project_full_path, default_branch|
      if gitlab_api_service.has_gitlab_ci_file?(project_id, default_branch)
        puts "Assigning security policy project #{security_policy_project_id} to #{project_full_path}"
        gitlab_api_service.assign_security_policy_project(project_full_path, security_policy_project_id)
      else
        puts "Unassigning security policy project from #{project_full_path}"
        gitlab_api_service.unassign_security_policy_project(project_full_path)
      end
    end
  end

  private

  attr_reader :gitlab_api_service
end

AutoAssignSecurityPolicyProject
  .new(gitlab_api_service: GitlabApiService.new)
  .execute(ENV['AUTO_ASSIGN_SECURITY_POLICY_GROUP_ID'], ENV['AUTO_ASSIGN_SECURITY_POLICY_PROJECT_ID'])
