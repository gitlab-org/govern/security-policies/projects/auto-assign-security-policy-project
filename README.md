# Auto Assign Security Policy Project

This project contains script to automatically assign selected Security Policy Project to all projects in selected group that has `.gitlab-ci.yml` file and unassign it when this file is missing.

## How to start?

1. Ensure you are owner of the group where you want to assign Security Policy Project to your projects.
1. Ensure you have Security Policy Project created in the group (ie. create new Scan Execution Policy for one of your projects in the group, it will automatically create Security Policy Project).
1. Create new project in that group.
1. In your group, create the access token with a Owner role and `api` and `read_repository` scopes. You will use the value of the token in the following steps.
    Add these CI/CD variables to your project:
    * `GITLAB_TOKEN`: The token value you created in the previous step. Mark the variable as masked and protected.
    * `AUTO_ASSIGN_SECURITY_POLICY_GROUP_ID`: The id of the group you'd like to use to auto-assign/unassign security policy project. Go to your group page, find ⋮ icon on the right side of the `New Project` button, and select `Copy group ID`.
    * `AUTO_ASSIGN_SECURITY_POLICY_PROJECT_ID`: The id of the project you'd like to use to as security policy project. Go to your security policy project page, find ⋮ icon on the right side of the `New Project` button, and select `Copy project ID`.
1. Create a `.gitlab-ci.yml` file with the following contents:
   ```
   include:
   - https://gitlab.com/gitlab-org/security-risk-management/security-policies/projects/auto-assign-security-policy-project/-/raw/main/templates/.gitlab-ci.yml
   ```
1. [Create a new pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) and add the variable `AUTO_ASSIGN_SECURITY_POLICY_PROJECT_ENABLED` set to `true`.
1. Test this automation by [running the schedule pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#run-manually). You should see in the logs information about assigned/unassigned projects.
